# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\u \W> '
setxkbmap -model abnt2 -layout br
(cat ~/.cache/wal/sequences &)
